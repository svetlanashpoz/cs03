﻿using System;

namespace CoreHWApp
{
	public class Program
	{
		public static void Main(string[] args)
		{
        
			string str;

			int firstsign, secondsign;

			Console.WriteLine("Hello, dear user!");

			Console.WriteLine("I'll help you solve some simple eguations :)");

			Console.WriteLine("First eguation is (cos x - cos y)^2 - (sin x - sin y)^2");

			Console.WriteLine("Please, entre a value X");

			str = Console.ReadLine();

			firstsign = Convert.ToInt32(str);

			Console.WriteLine("Please, entre a value Y");

			str = Console.ReadLine();

			secondsign = Convert.ToInt32(str);

			double a = Convert.ToDouble(str);

			a = Math.Cos(firstsign * Math.PI / 180) - Math.Cos(secondsign * Math.PI / 180);   // *переводим радианы

			double b = Convert.ToDouble(str);

			b = Math.Sin(firstsign * Math.PI / 180) - Math.Sin(secondsign * Math.PI / 180);

			double result = Math.Pow(a, 2) - Math.Pow(b, 2);

			Console.WriteLine("Your result = " + result);

			// Второй пример

			Console.WriteLine("Second eguation is (sin 4x /(1 + cos 4x)) * (cos 2y / (1 + cos 2y))");

			int firstsign1, secondsign2;

			Console.WriteLine("Please, entre a value X");

			str = Console.ReadLine();

			firstsign1 = Convert.ToInt32(str);

			Console.WriteLine("Please, entre a value Y");

			str = Console.ReadLine();

			secondsign2 = Convert.ToInt32(str);

			double a1 = Convert.ToDouble(str);

			a1 = Math.Sin(firstsign1 * Math.PI / 180)*4 / Math.Cos(firstsign1 * Math.PI / 180)*4 + 1;   // *переводим радианы

			double b1 = Convert.ToDouble(str);

			b1 = Math.Cos(secondsign2 * Math.PI / 180) * 2 / Math.Cos(secondsign2 * Math.PI / 180) * 2 + 1;

			double result2 = a1 * b1;

			Console.WriteLine("Your result = " + result2);

			// Пример 3

			Console.WriteLine("Third eguation is sqrt((3x+2)^2 – 24x)) / (3*sqrt y – (2/sqrt y))");

			int firstsign3, secondsign3;

			Console.WriteLine("Please, entre a value X");

			str = Console.ReadLine();

			firstsign3 = Convert.ToInt32(str);

			Console.WriteLine("Please, entre a value Y");

			str = Console.ReadLine();

			secondsign3 = Convert.ToInt32(str);

			double a3 = Convert.ToDouble(str);

			a3 = Math.Sqrt(Math.Pow(firstsign3 * 3 + 2, 2) - 24 * firstsign3);

			double b3 = Convert.ToDouble(str);

			b3 = 3 * Math.Sqrt(secondsign3) - (2/ Math.Sqrt(secondsign3));

			double result3 = a3 / b3;

			Console.WriteLine("Your result = " + result3);

			// Пример 4

			Console.WriteLine("Fourth eguation is ((x-1)*sqrt x – (y-1)*sqrt y) / ((sqrt (x^3*y))+xy+x^2-x)");

			int firstsign4, secondsign4;

			Console.WriteLine("Please, entre a value X");

			str = Console.ReadLine();

			firstsign4 = Convert.ToInt32(str);

			Console.WriteLine("Please, entre a value Y");

			str = Console.ReadLine();

			secondsign4 = Convert.ToInt32(str);

			double a4 = Convert.ToDouble(str);

			a4 = firstsign4 - 1 * Math.Sqrt(firstsign4) - (secondsign4 - 1) * Math.Sqrt(secondsign4);

			double b4 = Convert.ToDouble(str);

			b4 = Math.Sqrt(Math.Pow (firstsign4, 3) * secondsign4) + firstsign4 * secondsign4 + Math.Pow(firstsign4, 2) - firstsign4;

			double result4 = a4 / b4;

			Console.WriteLine("Your result = " + result4);
		}
	}
}